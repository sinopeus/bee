% Author: Amit Metodi
% Last Update: 01/07/2012

:- module(bcExactlyOne, [ ]).
:- use_module('../auxs/auxLiterals').

%%% ------------------------- %%%
%%% constraints types         %%%
%%% ------------------------- %%%
exoType([
         bcExactlyOne:exoSimplify,
         skipSimplify,
         0,
         bcExactlyOne:exo2cnf,
         exactlyOne
        ]):-!.

% -------------
% | Simplify  |
% -------------
exoSimplify(Constr,NewConstr,Changed):-!,
    Constr=bc(Type,Vec),
    atMostOne:atMostOneSimplify(Vec,NVec,FoundOne,Changed),
    (ground(FoundOne) -> %(FoundOne==1 ; FoundOne==2) ->
        Changed=1,
        NewConstr=none ;
    (NVec=[A,B] ->
        Changed=1,
        plitUnifyDiff(A,B),
        NewConstr=none ;
    (NVec=[PBit] ->
        Changed=1,
        plitAsgnTrue(PBit),
        NewConstr=none ;
    (NVec=[] ->
        throw(unsat) ;
    (Changed==1 ->
        NewConstr=bc(Type,NVec)
    ;
        NewConstr=Constr
    ))))).


:- if(bSettings:exactlyOneEncoding(standard)).

exo2cnf(bc(_Type,Vec),CnfH-CnfT):-!,
    plits2lits(Vec,Xs),!,
    length(Xs,N),!,
    (N > 9 ->
        bcAtMostOne:atMostOne2clauses(Xs,N,CnfH-CnfM),
        bcAtLeastOne:atLeastOne2clauses(Xs,N,CnfM-CnfT)
    ;
        exoDirectCnf(Xs,CnfH-CnfT)
    ).

:- elif(bSettings:exactlyOneEncoding(log)).

exo2cnf(bc(_Type,Vec),CnfH-CnfT):-!,
    plits2lits(Vec,Xs),!,
    length(Xs,N),!,
    (N > 9 ->
        exoLogCnf(Xs,N,CnfH-CnfT)
    ;
        exoDirectCnf(Xs,CnfH-CnfT)
    ).


exoLogCnf(Xs,N,CnfH-CnfT):-!,
   calcParts(N,Parts,0,PartsCnt),
   exoLogCnf_(Parts,Xs,Ys,CnfH-CnfM),!,
   (PartsCnt > 9 ->
       exoLogCnf(Ys,PartsCnt,CnfM-CnfT)
   ;
       exoDirectCnf(Ys,CnfM-CnfT)
   ).

exoLogCnf_([_],Xs,[Y],CnfH-CnfT):-!,
    append(Xs,[-Y],XsY),!,
    exoDirectCnf(XsY,CnfH-CnfT).
exoLogCnf_([N|Parts],Xs,[Yi|Ys],CnfH-CnfT):-!,
    auxLists:listSplit(N,Xs,XsI,RXs),!,
    append(XsI,[-Yi],XsY),
    exoDirectCnf(XsY,CnfH-CnfM),!,
    exoLogCnf_(Parts,RXs,Ys,CnfM-CnfT).


calcParts(0,[],Cnt,Cnt):-!.
calcParts(N,[N],I,Cnt):-
   N =< 8, !,
   Cnt is I + 1.
calcParts(N,[PartSize|Ss],I,Cnt):-
   Parts is ceil(N/8),
   PartSize is ceil(N / Parts),
   NN is N - PartSize,
   I1 is I + 1,
   calcParts(NN,Ss,I1,Cnt).
:- else.
:- bSettings:exactlyOneEncoding(X), writef('ERROR: "%w" is wrong value for bSettings:exactlyOneEncoding !\n',[X]),flush_output,halt.
:- endif.

exoDirectCnf(Xs,CnfH-CnfT):-!,
    bcAtMostOne:atMostOneDirectCnf(Xs,CnfH-[Xs|CnfT]).
% Authors: Amit Metodi and Yoav Fekete
% Last Updated: 25/07/2012

:- module(bcSumIntsModK, [ ]).

%%% ------------------------- %%%
%%% add constraints to parser %%%
%%% ------------------------- %%%
:- Head=int_array_sum_modK(Ints,K,Sum,ConstrsH-ConstrsT),
   Body=(
       !,
       ((integer(K), K>0) ->
           length(Ints,N),
           bcSumIntsModK:decomposeSumModK(Ints,N,K,SumT,ConstrsH-ConstrsT),
           bcInteger:getUnaryNumber(Sum,SumU),
           auxUnarynum:unarynumEquals(SumT,SumU)
       ;
           throw(unsupported(int_array_sum_modK(intVars,not(posConst),intVar)))
       )
   ),
   bParser:addConstraint(Head,Body).
   
  
%%% ------------------------- %%%
%%% decompoosition            %%%
%%% ------------------------- %%%
decomposeSumModK(Ints,N,K,Sum,ConstrsH-ConstrsT):-
    N==1,!,% base cases
    Ints=[I1],
    bcInteger:getUnaryNumber(I1,I1U),
    I1U=(Min,Max,_),
    %% Assert : possitive numbers only
    !,Min >= 0,!,
    (Max<K ->
        Sum=I1U,
        ConstrsH=ConstrsT
    ;
        K1 is K -1,
        bParser:new_int(SumInt,0,K1,ConstrsH-ConstrsM),
        bcInteger:getUnaryNumber(SumInt,Sum),
        bParser:int_mod(I1,K,SumInt,ConstrsM-ConstrsT)
    ).

decomposeSumModK(Ints,N,K,Sum,ConstrsH-ConstrsT):-
    N2 is N // 2,
    N1 is N - N2,
    auxLists:listSplit(N1,Ints,Ints1,Ints2),
    decomposeSumModK(Ints1,N1,K,Sum1,ConstrsH-Constrs1),
    decomposeSumModK(Ints2,N2,K,Sum2,Constrs1-Constrs2),
    Sum3len is 2*K-2,
    auxUnarynum:unarynumNewInRange(0,Sum3len,Sum3),
    bcUnaryAdder:uadderSimplify1st(bc(_,[Sum1, Sum2, Sum3]), Constr, _),
    (Constr==none ->
        Constrs2=Constrs3
    ;
        Constrs2=[Constr|Constrs3]
    ),
    specialCaseMod(Sum3,K,Sum,Constrs3-ConstrsT).
   
   
% where K<=|X|< 2*K
specialCaseMod(X,K,XmodK,ConstrsH-ConstrsT):-
    X=(Min,Max,Bits,_),
    %assert for this code:
    !,Min==0, Max < 2*K,!,
    K1 is K - 1,
    auxLists:listSplit(K1,Bits,Bits1,[Y|Bits2]),
    auxUnarynum:unarynumNewInRange(0,K1,XmodK),
    XmodK=(0,K1,Bits3,_),
    bcBoolElement:boolElementType(TypeMux),!,
    mux2vecs(Bits1,Bits2,[-Y,Y],Bits3,TypeMux,ConstrsH-ConstrsT).

mux2vecs([X|Xs],[Y|Ys],I,[Z|Zs],Type,[bc(Type,[I,[X,Y],Z])|ConstrsH]-ConstrsT):-!,
    mux2vecs(Xs,Ys,I,Zs,Type,ConstrsH-ConstrsT).
mux2vecs([],[],_,[],_,Constrs-Constrs):-!.
mux2vecs([],[Y|Ys],I,[Z|Zs],Type,[bc(Type,[I,[-1,Y],Z])|ConstrsH]-ConstrsT):-!,
    mux2vecs([],Ys,I,Zs,Type,ConstrsH-ConstrsT).
mux2vecs([X|Xs],[],I,[Z|Zs],Type,[bc(Type,[I,[X,-1],Z])|ConstrsH]-ConstrsT):-!,
    mux2vecs(Xs,[],I,Zs,Type,ConstrsH-ConstrsT).
mux2vecs([X|Xs],[Y|Ys],I,[],Type,[bc(Type,[I,[X,Y],-1])|ConstrsH]-ConstrsT):-!,
    mux2vecs(Xs,Ys,I,[],Type,ConstrsH-ConstrsT).
mux2vecs([X|Xs],[],I,[],Type,[bc(Type,[I,[X,-1],-1])|ConstrsH]-ConstrsT):-!,
    mux2vecs(Xs,[],I,[],Type,ConstrsH-ConstrsT).
mux2vecs([],[Y|Ys],I,[],Type,[bc(Type,[I,[-1,Y],-1])|ConstrsH]-ConstrsT):-!,
    mux2vecs([],Ys,I,[],Type,ConstrsH-ConstrsT).
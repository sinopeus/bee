@echo off
echo =================================================
echo =====         Compile  PL-SATsolver         =====
echo =================================================
echo Please Make sure:
echo 1. SWI-Prolog v6.0.x or above is installed 
echo    and accessable from every path.
echo 2. Executing of this command file is done
echo    from Visual Studio Command Prompt !
echo    or
echo    A call to set up Visual Studio
echo    environment was added to this file.
echo =================================================
pause
rem echo ---- SET VS ENVIRONMENT -----
rem call "C:\Program Files\Microsoft Visual Studio 9.0\VC\vcvarsall.bat" x86
rem call "C:\Program Files\Microsoft Visual Studio 9.0\VC\vcvarsall.bat" x86_amd64

echo ---- EXTRACT SOURCE ----
md ../satsolver
TarTool plMiniSAT_src.tar.gz ../satsolver
TarTool plSATsolver.tar.gz ../satsolver
echo ---- COMPILE -----
(cd ..\satsolver\prologinterface & CMD /C make.cmd & cd ..\..\satsolver_src)

echo ---- CLEAN SOURCE ----
rd /s /q ..\satsolver\minisat-2.0.2
rd /s /q ..\satsolver\prologinterface

echo ---- DONE -----
